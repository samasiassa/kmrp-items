package de.cas_ual_ty.gci.network;

import de.cas_ual_ty.gci.ContainerArmorTable;
import de.cas_ual_ty.gci.ContainerGunTable;
import de.cas_ual_ty.gci.client.GuiArmorTable;
import de.cas_ual_ty.gci.client.GuiGunTable;
import de.cas_ual_ty.gci.inventory.ContainerCustomInv;
import de.cas_ual_ty.gci.inventory.GUICustomInv;
import de.cas_ual_ty.gci.inventory.bigcontainer.ContainerBigCont;
import de.cas_ual_ty.gci.inventory.bigcontainer.GUIContainerBig;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.CAPContainerBig;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.CAPContainerBigProvider;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.ICAPContainerBig;
import de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider;
import de.cas_ual_ty.gci.inventory.capabilities.ICAPCustomInventory;
import de.cas_ual_ty.gci.inventory.inventory.ContainerCont;
import de.cas_ual_ty.gci.inventory.inventory.GUIContainer;
import de.cas_ual_ty.gci.inventory.inventory.capabilities.CAPContainerProvider;
import de.cas_ual_ty.gci.inventory.inventory.capabilities.ICAPContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandlerGCI implements IGuiHandler
{
	@Override
	public Object getClientGuiElement(int id, EntityPlayer entityPlayer, World world, int x, int y, int z)
	{
		switch(id)
		{
			case 0: return new GuiGunTable(new ContainerGunTable(entityPlayer, world, new BlockPos(x, y, z)));
			case 1: return new GuiArmorTable(new ContainerArmorTable(entityPlayer, world, new BlockPos(x, y, z)));
			case 2: ICAPCustomInventory inv = entityPlayer.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
				return new GUICustomInv(entityPlayer, entityPlayer.inventory, inv.getInventory());
			case 3:         ICAPContainer inv2 = entityPlayer.getHeldItemMainhand().getCapability(CAPContainerProvider.CONTAINER_CAP, null);
				if(inv2 != null && entityPlayer.getHeldItemMainhand().getCount() == 1) {
					return new GUIContainer(entityPlayer, entityPlayer.inventory, inv2.getInventory());
				}
				return null;
			case 4:         ICAPContainerBig inv3 = entityPlayer.getHeldItemMainhand().getCapability(CAPContainerBigProvider.CONTAINERBIG_CAP, null);
				if(inv3 != null && entityPlayer.getHeldItemMainhand().getCount() == 1) {
					return new GUIContainerBig(entityPlayer, entityPlayer.inventory, inv3.getInventory());
				}
				return null;
		}
		
		return null;
	}
	
	@Override
	public Object getServerGuiElement(int id, EntityPlayer entityPlayer, World world, int x, int y, int z)
	{
		switch(id)
		{
			case 0: return new ContainerGunTable(entityPlayer, world, new BlockPos(x, y, z));
			case 1: return new ContainerArmorTable(entityPlayer, world, new BlockPos(x, y, z));
			case 2: ICAPCustomInventory inv = entityPlayer.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
				return new ContainerCustomInv(entityPlayer.inventory, inv.getInventory(), entityPlayer);
			case 3: ICAPContainer inv2 = entityPlayer.getHeldItemMainhand().getCapability(CAPContainerProvider.CONTAINER_CAP, null);
				if(inv2 != null && entityPlayer.getHeldItemMainhand().getCount() == 1) {
					return new ContainerCont(entityPlayer.inventory, inv2.getInventory(), entityPlayer);
				}
				return null;
			case 4: ICAPContainerBig inv3 = entityPlayer.getHeldItemMainhand().getCapability(CAPContainerBigProvider.CONTAINERBIG_CAP, null);
				if(inv3 != null && entityPlayer.getHeldItemMainhand().getCount() == 1) {
					return new ContainerBigCont(entityPlayer.inventory, inv3.getInventory(), entityPlayer);
				}
				return null;
		}
		
		return null;
	}
	
}
