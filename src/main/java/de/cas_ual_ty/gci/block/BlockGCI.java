package de.cas_ual_ty.gci.block;

import java.util.ArrayList;

import javax.annotation.Nullable;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.BlockRenderLayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockGCI extends Block
{
	public static final ArrayList<BlockGCI> BLOCKS_LIST = new ArrayList<BlockGCI>();
	
	private String modelRL;
	
	public BlockGCI(String rl, Material material)
	{
		this(rl, null, material);
	}
	
	public BlockGCI(String rl, @Nullable CreativeTabs tab, Material material)
	{
		super(material);
		
		this.setUnlocalizedName(GunCus.MOD_ID + ":" + rl);
		this.setRegistryName(GunCus.MOD_ID + ":" + rl);
		this.modelRL = rl;

		if(tab != null)
		{
			this.setCreativeTab(tab);
		}
		else
		{
			this.setCreativeTab(GunCus.TAB_GUNCUS);
		}

//		System.out.println("Adding block: " + this.getUnlocalizedName() + ", "  + getRegistryName());
		BlockGCI.BLOCKS_LIST.add(this);
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
	    return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	public String getModelRL()
	{
		return this.modelRL;
	}
}
