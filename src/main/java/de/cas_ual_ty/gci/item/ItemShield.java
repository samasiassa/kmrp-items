package de.cas_ual_ty.gci.item;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;

import javax.annotation.Nullable;

public class ItemShield extends ItemGCI {



    private String modelRL;

    public ItemShield(String rl)
    {
        this(rl, null);
    }

    public ItemShield(String rl, @Nullable CreativeTabs tab)
    {
        super(rl, GunCus.KMRP_TAB);
        this.setMaxStackSize(1);
    }

}
