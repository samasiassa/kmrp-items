package de.cas_ual_ty.gci.inventory.inventory.capabilities;

import de.cas_ual_ty.gci.inventory.inventory.ContainerC;

public class CAPContainer implements ICAPContainer {

    //Реализуем методы, что определены в интерфейсе ICAPCustomInventory

    //Создаем обьект нашего инвентаря. Он будет храниться в этой КАП'е
    public final ContainerC inventory = new ContainerC();

    /**
     * Метод, который возвращает обьект инвентаря inventory
     */
    public ContainerC getInventory(){
        return this.inventory;
    }

    /**
     * Метод, для копировании информации из другого инвентаря, например при клонировании
     */
    @Override
    public void copyInventory(ICAPContainer inventory) {
        this.inventory.copy(inventory.getInventory());
    }

}
