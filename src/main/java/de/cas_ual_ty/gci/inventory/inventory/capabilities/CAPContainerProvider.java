package de.cas_ual_ty.gci.inventory.inventory.capabilities;



import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class CAPContainerProvider implements ICapabilitySerializable<NBTBase> {

    //Инициализация КАПы с помощью аннотации
    @CapabilityInject(ICAPContainer.class)
    public static final Capability<ICAPContainer> CONTAINER_CAP = null;

    private ICAPContainer instance = CONTAINER_CAP.getDefaultInstance();

    //Метод что осуществляет проверку на наличие КАПы
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CONTAINER_CAP;
    }

    //Метод что осуществляет доступ к КАПе
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        return capability == CONTAINER_CAP ? CONTAINER_CAP.<T> cast(this.instance) : null;
    }

    //Метод инициации сохранения информации о инвентаре в НБТ
    @Override
    public NBTBase serializeNBT() {
        return CONTAINER_CAP.getStorage().writeNBT(CONTAINER_CAP, this.instance, null);
    }

    //Метод инициации чтения информации о инвентаре из НБТ
    @Override
    public void deserializeNBT(NBTBase nbt) {
        CONTAINER_CAP.getStorage().readNBT(CONTAINER_CAP, this.instance, null, nbt);
    }

}


