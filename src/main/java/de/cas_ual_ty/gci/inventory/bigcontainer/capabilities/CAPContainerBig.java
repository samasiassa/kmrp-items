package de.cas_ual_ty.gci.inventory.bigcontainer.capabilities;

import de.cas_ual_ty.gci.inventory.bigcontainer.ContainerBigC;

public class CAPContainerBig implements ICAPContainerBig {

    //Реализуем методы, что определены в интерфейсе ICAPCustomInventory

    //Создаем обьект нашего инвентаря. Он будет храниться в этой КАП'е
    public final ContainerBigC inventory = new ContainerBigC();

    /**
     * Метод, который возвращает обьект инвентаря inventory
     */
    public ContainerBigC getInventory(){
        return this.inventory;
    }

    /**
     * Метод, для копировании информации из другого инвентаря, например при клонировании
     */
    @Override
    public void copyInventory(ICAPContainerBig inventory) {
        this.inventory.copy(inventory.getInventory());
    }

}
