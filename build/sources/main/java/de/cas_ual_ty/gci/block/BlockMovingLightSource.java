package de.cas_ual_ty.gci.block;


import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.attachment.*;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static de.cas_ual_ty.gci.GunCus.MOVING_LIGHT_SOURCE;


/**
 * @author jabelar
 *
 */
public class BlockMovingLightSource extends BlockGCI implements ITileEntityProvider
{
    public static AxisAlignedBB boundingBox = new AxisAlignedBB(0.5D, 0.5D, 0.5D, 0.5D, 0.5D, 0.5D);
    private String modelRL;

    public BlockMovingLightSource(String rl) {
        super(rl, Material.AIR);
        setDefaultState(blockState.getBaseState());
        setTickRandomly(false);
        setLightLevel(1.0F);
        // setBlockBounds(0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F);

    }


    public static boolean isHoldingLightItem(EntityLivingBase parLivingBase)
    {
        //work in progress
        ItemStack itemStack;
        ItemGun gun;
        Underbarrel underbarrel;
        Ammo rightside;
        Auxiliary leftside;
        for(EnumHand hand : EnumHand.values())
        {
            itemStack = parLivingBase.getHeldItem(hand);
            if(itemStack.getItem() instanceof ItemGun)
            {
                gun = (ItemGun) itemStack.getItem();
                underbarrel = gun.<Underbarrel>getAttachmentCalled(itemStack, EnumAttachmentType.UNDERBARREL.getSlot());
                if (underbarrel.isFlashActive()) {
                    return true;
                }
                leftside = gun.<Auxiliary>getAttachmentCalled(itemStack, EnumAttachmentType.AUXILIARY.getSlot());
                if (leftside.isFlashActive()) {
                    return true;
                }
                rightside = gun.<Ammo>getAttachmentCalled(itemStack, EnumAttachmentType.AMMO.getSlot());
                if (rightside.isFlashActive()) {
                    return true;
                }


            }
            if(itemStack.getItem() instanceof Attachment) {
                if(itemStack.getItem() instanceof Underbarrel)
                {
                    underbarrel = (Underbarrel) itemStack.getItem();
                    if (underbarrel.isFlashActive()) {
                        return true;
                    }
                }
                if(itemStack.getItem() instanceof Auxiliary)
                {
                    leftside = (Auxiliary) itemStack.getItem();
                    if (leftside.isFlashActive()) {
                        return true;
                    }
                }
                if(itemStack.getItem() instanceof Ammo)
                {
                    rightside = (Ammo) itemStack.getItem();
                    if (rightside.isFlashActive()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static Block lightBlockToPlace(EntityLivingBase parEntityLivingBase)
    {
        if (parEntityLivingBase == null)
        {
            return Blocks.AIR;
        }

        return MOVING_LIGHT_SOURCE;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        return boundingBox;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return NULL_AABB;
    }

    @Override
    public boolean canCollideCheck(IBlockState state, boolean hitIfLiquid)
    {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState parIBlockState)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState parIBlockState)
    {
        return false;
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
    {
        return true;
    }

    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
    {
        return;
    }

    /**
     * Called when a neighboring block changes.
     */
    @Override
    public void onNeighborChange(IBlockAccess worldIn, BlockPos pos, BlockPos neighborPos)
    {
        return;
    }

    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return getDefaultState();
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        return 0;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public void onFallenUpon(World worldIn, BlockPos pos, Entity entityIn, float fallDistance)
    {
        // want entities to be able to fall through it
        return;
    }

    @Override
    public void onLanded(World worldIn, Entity entityIn)
    {
        return;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta)
    {
        TileEntityMovingLightSource theTileEntity = new TileEntityMovingLightSource();
        return theTileEntity;
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }
}
